﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD.Telas.Agenda
{
    public partial class frmDeletarAgenda : Form
    {
        public frmDeletarAgenda()
        {
            InitializeComponent();
        }

        private void btnDeletar_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtId.Text);

            Business.AgendaBusiness agendaBusiness = new Business.AgendaBusiness();
            agendaBusiness.Deletrar(id);

            MessageBox.Show("Deletado com sucesso");
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtId.Text);

            Business.AgendaBusiness agendabusiness = new Business.AgendaBusiness();
            Model.AgendaModel model = agendabusiness.FiltrarPorID(id);

            if (model != null)
            {
                txtNome.Text = model.Nome;
                txtContato.Text = model.Contato;
            }
            else
            {
                txtNome.Text = string.Empty;
                txtContato.Text = string.Empty;
            }
        }
    }
}
