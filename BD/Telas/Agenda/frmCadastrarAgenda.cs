﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD.Telas
{
    public partial class frmCadastrarAgenda : Form
    {
        public frmCadastrarAgenda()
        {
            InitializeComponent();
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                Model.AgendaModel agenda = new Model.AgendaModel();
                agenda.Nome = txtNome.Text;
                agenda.Contato = txtContato.Text;

                Business.AgendaBusiness filmeBusiness = new Business.AgendaBusiness();
                filmeBusiness.Inserir(agenda);

                MessageBox.Show("Agenda inserido com sucesso");
            } 
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception )
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }
        }
    }
}
