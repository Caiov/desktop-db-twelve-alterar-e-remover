﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD.Telas.Agenda
{
    public partial class frmAlterarAgenda : Form
    {
        public frmAlterarAgenda()
        {
            InitializeComponent();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Model.AgendaModel agenda = new Model.AgendaModel();
            agenda.Id = Convert.ToInt32(txtId.Text);
            agenda.Nome = txtNome.Text;
            agenda.Contato = txtContato.Text;

            Business.AgendaBusiness agendabusiness = new Business.AgendaBusiness();
            agendabusiness.Alterar(agenda);

            MessageBox.Show("Alterado com sucesso");
        }

    }
}
