﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD.Telas.Pensamentos
{
    public partial class frmDeletarPensamento: Form
    {
        public frmDeletarPensamento()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtID.Text);

            Business.PensamentoBusiness pensamentoBusiness = new Business.PensamentoBusiness();
            Model.PensamentoModel model = pensamentoBusiness.FiltrarPorID(id);

            if(model != null)
            {
                cboPolaridade.Text = model.Polaridade;
                txtSentimento.Text = model.Sentimento;
                txtGrupoSocial.Text = model.GrupoSocial;
            }
            else
            {
                cboPolaridade.Text = string.Empty;
                txtSentimento.Text = string.Empty;
                txtGrupoSocial.Text = string.Empty;
            }
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtID.Text);

            Business.PensamentoBusiness pensamentoDatabase = new Business.PensamentoBusiness();
            pensamentoDatabase.Deletar(id);

            MessageBox.Show("Deletado com sucesso");
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
