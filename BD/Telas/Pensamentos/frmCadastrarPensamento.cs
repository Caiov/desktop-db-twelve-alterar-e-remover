﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD.Telas.Pensamentos
{
    public partial class frmCadastrarPensamento : Form
    {
        public frmCadastrarPensamento()
        {
            InitializeComponent();
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            Model.PensamentoModel pensamento = new Model.PensamentoModel();
            pensamento.Polaridade = cboPolaridade.Text;
            pensamento.Sentimento = txtSentimento.Text;
            pensamento.GrupoSocial = txtGrupoSocial.Text;
            pensamento.Inclusao = DateTime.Now;

            Business.PensamentoBusiness pensamentoBusiness = new Business.PensamentoBusiness();
            pensamentoBusiness.Inserir(pensamento);

            MessageBox.Show("Inserido com sucesso");
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
