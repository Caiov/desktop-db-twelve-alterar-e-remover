﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD.Telas.Pensamentos
{
    public partial class frmConsultarPensamentos : Form
    {
        public frmConsultarPensamentos()
        {
            InitializeComponent();
        }

        private void cboPolaridade_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Buscar();
        }

        private void dtpDia_ValueChanged(object sender, EventArgs e)
        {
            this.Buscar();
        }

        private void Buscar()
        {
            string polaridade = cboPolaridade.Text;
            DateTime dia = dtpDia.Value.Date;

            Business.PensamentoBusiness pensamentoBusiness = new Business.PensamentoBusiness();
            List<Model.PensamentoModel> lista = pensamentoBusiness.Filtrar(polaridade, dia);

            dgvPensamentos.DataSource = lista;
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
