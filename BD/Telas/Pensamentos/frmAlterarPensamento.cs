﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD.Telas.Pensamentos
{
    public partial class frmAlterarPensamento : Form
    {
        public frmAlterarPensamento()
        {
            InitializeComponent();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            Model.PensamentoModel model = new Model.PensamentoModel();
            model.Id = Convert.ToInt32(txtId.Text);
            model.Polaridade = cboPolaridade.Text;
            model.Sentimento = txtSentimento.Text;
            model.GrupoSocial = txtGrupoSocial.Text;

            Business.PensamentoBusiness pensamentoBusiness = new Business.PensamentoBusiness();
            pensamentoBusiness.Alterar(model);

            MessageBox.Show("Alterado com sucesso");
        }
    }
}
