﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD.Telas
{
    public partial class frmConsultarFilme : Form
    {
        public frmConsultarFilme()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            string nome = txtNome.Text;

            Business.FilmeBusiness filmeBusiness = new Business.FilmeBusiness();
            List<Model.FilmeModel> lista = filmeBusiness.FiltrarPorNome(nome);

            dgvFilmes.DataSource = lista;
        }
    }

}
