﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD.Telas
{
    public partial class frmCadastrarFilme : Form
    {
        public frmCadastrarFilme()
        {
            InitializeComponent();
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                Model.FilmeModel filme = new Model.FilmeModel();
                filme.Nome = txtNome.Text;
                filme.Avaliacao = nudAvaliacao.Value;
                filme.Estreia = dtpEstreia.Value;
                filme.Disponivel = chkDisponivel.Checked;

                Business.FilmeBusiness filmeBusiness = new Business.FilmeBusiness();
                filmeBusiness.Inserir(filme);

                MessageBox.Show("Filme inserido com sucesso");
            } 
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception )
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }
        }
    }
}
