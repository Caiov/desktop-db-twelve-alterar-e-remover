﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD.Telas
{
    public partial class frmAlterarFilme : Form
    {
        public frmAlterarFilme()
        {
            InitializeComponent();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Model.FilmeModel filme = new Model.FilmeModel();
            filme.ID = Convert.ToInt32(txtId.Text);
            filme.Nome = txtNome.Text;
            filme.Avaliacao = nudAvaliacao.Value;

            Business.FilmeBusiness filmeBusiness = new Business.FilmeBusiness();
            filmeBusiness.Alterar(filme);

            MessageBox.Show("Alterado com sucesso");
        }
    }
}
