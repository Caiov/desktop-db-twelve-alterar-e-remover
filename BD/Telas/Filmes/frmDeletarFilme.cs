﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD.Telas
{
    public partial class frmDeletarFilme : Form
    {
        public frmDeletarFilme()
        {
            InitializeComponent();
        }

        private void btnDeletar_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtId.Text);

            Business.FilmeBusiness filmeBusiness = new Business.FilmeBusiness();
            filmeBusiness.Remover(id);

            MessageBox.Show("Removido com sucesso");
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtId.Text);

            Business.FilmeBusiness filmebusiness = new Business.FilmeBusiness();
            Model.FilmeModel model = filmebusiness.FiltrarPorID(id);

            if(model != null)
            {
                txtNome.Text = model.Nome;
                nudAvaliacao.Value = model.Avaliacao;
            }
            else
            {
                txtNome.Text = string.Empty;
                nudAvaliacao.Value = 0;
            }
        }
    }
}
