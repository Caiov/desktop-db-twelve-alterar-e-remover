﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD.Telas
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrarFilme tela = new frmCadastrarFilme();
            tela.Show();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultarFilme tela = new frmConsultarFilme();
            tela.Show();
        }

        private void inserirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrarAgenda tela = new frmCadastrarAgenda();
            tela.Show();
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmConsultarAgenda tela = new frmConsultarAgenda();
            tela.Show();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Telas.Pensamentos.frmCadastrarPensamento tela = new Pensamentos.frmCadastrarPensamento();
            tela.Show();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Telas.Pensamentos.frmConsultarPensamentos tela = new Pensamentos.frmConsultarPensamentos();
            tela.Show();
        }

        private void alterarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.frmAlterarFilme tela = new frmAlterarFilme();
            tela.Show();
        }

        private void deletarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDeletarFilme tela = new frmDeletarFilme();
            tela.Show();
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void alterarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Agenda.frmAlterarAgenda tela = new Agenda.frmAlterarAgenda();
            tela.Show();
        }

        private void deletarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Agenda.frmDeletarAgenda tela = new Agenda.frmDeletarAgenda();
            tela.Show();
        }

        private void alterarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Pensamentos.frmAlterarPensamento tela = new Pensamentos.frmAlterarPensamento();
            tela.Show();
        }

        private void deletarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Pensamentos.frmDeletarPensamento tela = new Pensamentos.frmDeletarPensamento();
            tela.Show();
        }
    }
}
