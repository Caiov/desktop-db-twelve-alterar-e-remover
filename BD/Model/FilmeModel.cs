﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BD.Model
{
    class FilmeModel
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public decimal Avaliacao { get; set; }
        public bool Disponivel { get; set; }
        public DateTime Estreia { get; set; }

    }
}
