﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BD.Model
{
    class PensamentoModel
    {
        public int Id { get; set; }
        public string Polaridade { get; set; }
        public string Sentimento { get; set; }
        public string GrupoSocial { get; set; }
        public DateTime Inclusao { get; set; }
    }
}
