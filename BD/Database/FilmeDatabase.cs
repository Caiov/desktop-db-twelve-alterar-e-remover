﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BD.Database
{
    class FilmeDatabase
    {

        public void Inserir(Model.FilmeModel filme)
        {
            string script = @"insert into tb_filme (nm_filme, vl_avaliacao, bt_disponivel, dt_estreia)
                                            values (@nm_filme, @vl_avaliacao, @bt_disponivel, @dt_estreia)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_filme", filme.Nome));
            parms.Add(new MySqlParameter("vl_avaliacao", filme.Avaliacao));
            parms.Add(new MySqlParameter("bt_disponivel", filme.Disponivel));
            parms.Add(new MySqlParameter("dt_estreia", filme.Estreia));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }


        public List<Model.FilmeModel> FiltrarPorNome(string nome)
        {
            string script = "select * from tb_filme where nm_filme like @nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nome", "%" + nome + "%"));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Model.FilmeModel> lista = new List<Model.FilmeModel>();

            while (reader.Read())
            {
                Model.FilmeModel filme = new Model.FilmeModel();
                filme.ID = Convert.ToInt32(reader["id_filme"]);
                filme.Nome = Convert.ToString(reader["nm_filme"]);
                filme.Avaliacao = Convert.ToDecimal(reader["vl_avaliacao"]);
                filme.Estreia = Convert.ToDateTime(reader["dt_estreia"]);
                filme.Disponivel = Convert.ToBoolean(reader["bt_disponivel"]);

                lista.Add(filme);
            }
            reader.Close();

            return lista;
        }
        public Model.FilmeModel FiltrarPorID(int id)
        {
            string script = "select * from tb_filme where id_filme = @id_filme";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_filme", id));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            Model.FilmeModel model = null;

            if (reader.Read())
            {
                model = new Model.FilmeModel();
                model.ID = Convert.ToInt32(reader["id_filme"]);
                model.Nome = Convert.ToString(reader["nm_filme"]);
                model.Avaliacao = Convert.ToDecimal(reader["vl_avaliacao"]);
                model.Estreia = Convert.ToDateTime(reader["dt_estreia"]);
                model.Disponivel = Convert.ToBoolean(reader["bt_disponivel"]);
            }
            reader.Close();

            return model;
        }
        public void Alterar (Model.FilmeModel filme)
        {
            string script = @"update tb_filme
                                    set nm_filme = @nm_filme, 
                                    vl_avaliacao = @vl_avaliacao 
                                    where id_filme = @id_filme";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_filme", filme.ID));
            parms.Add(new MySqlParameter("nm_filme", filme.Nome));
            parms.Add(new MySqlParameter("vl_avaliacao", filme.Avaliacao));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"delete from tb_filme where id_filme = @id_filme";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_filme", id));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }
    }
}
