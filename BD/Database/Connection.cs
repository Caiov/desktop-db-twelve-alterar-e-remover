﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BD.Database
{
    class Connection
    {
        public MySqlConnection Create()
        {
            string connectionString = "server=localhost;database=infoB02_2019DB;uid=root;password=";

            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            return connection;
        }
    }
}
