﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BD.Database
{
    class AgendaDatabase
    {
        public void Inserir(Model.AgendaModel agenda)
        {
            string script = @"insert into tb_agenda (nm_pessoa, ds_contato)
                                             values (@nm_pessoa, @ds_contato)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_pessoa", agenda.Nome));
            parms.Add(new MySqlParameter("ds_contato", agenda.Contato));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }

        public List<Model.AgendaModel> Filtrar(string nome)
        {
            string script = "select * from tb_agenda where nm_pessoa like @nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nome", "%" + nome + "%"));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Model.AgendaModel> lista = new List<Model.AgendaModel>();

            while (reader.Read())
            {
                Model.AgendaModel model = new Model.AgendaModel();
                model.Id = Convert.ToInt32(reader["id_agenda"]);
                model.Nome = Convert.ToString(reader["nm_pessoa"]);
                model.Contato = Convert.ToString(reader["ds_contato"]);

                lista.Add(model);
            }
            reader.Close();

            return lista;
        }
        public Model.AgendaModel FiltrarPorID(int id)
        {
            string script = "select * from tb_agenda where id_agenda = @id_agenda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_agenda", id));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            Model.AgendaModel model = null;

            if (reader.Read())
            {
                model = new Model.AgendaModel();
                model.Id = Convert.ToInt32(reader["id_agenda"]);
                model.Nome = Convert.ToString(reader["nm_pessoa"]);
                model.Contato = Convert.ToString(reader["ds_contato"]);
            }
            reader.Close();

            return model;
        }

        public void Alterar (Model.AgendaModel agenda)
        {
            string script = @"update tb_agenda
                                        set nm_pessoa = @nm_pessoa,
                                            ds_contato = @ds_contato
                                      where id_agenda = @id_agenda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_agenda", agenda.Id));
            parms.Add(new MySqlParameter("nm_pessoa", agenda.Nome));
            parms.Add(new MySqlParameter("ds_contato", agenda.Contato));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }

        public void Deletar (int id)
        {
            string script = @"delete from tb_agenda where id_agenda = @id_agenda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_agenda", id));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }
    }
}
