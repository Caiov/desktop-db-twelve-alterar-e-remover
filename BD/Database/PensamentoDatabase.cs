﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BD.Database
{
    class PensamentoDatabase
    {
        public void Inserir(Model.PensamentoModel pensamento)
        {
            string script = @"insert into tb_pensamento (ds_polaridade, ds_sentimento, ds_gruposocial, dt_inclusao)
                                              values (@ds_polaridade, @ds_sentimento, @ds_gruposocial, @dt_inclusao)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_polaridade", pensamento.Polaridade));
            parms.Add(new MySqlParameter("ds_sentimento", pensamento.Sentimento));
            parms.Add(new MySqlParameter("ds_gruposocial", pensamento.GrupoSocial));
            parms.Add(new MySqlParameter("dt_inclusao", pensamento.Inclusao));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }

        public List<Model.PensamentoModel> Filtrar(string polaridade, DateTime dia)
        {
            string script = "select * from tb_pensamento where ds_polaridade = @ds_polaridade and dt_inclusao = @dt_dia";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_polaridade", polaridade));
            parms.Add(new MySqlParameter("dt_dia", dia));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Model.PensamentoModel> lista = new List<Model.PensamentoModel>();

            while (reader.Read())
            {
                Model.PensamentoModel model = new Model.PensamentoModel();
                model.Id = Convert.ToInt32(reader["id_pensamento"]);
                model.Polaridade = Convert.ToString(reader["ds_polaridade"]);
                model.Sentimento = Convert.ToString(reader["ds_sentimento"]);
                model.GrupoSocial = Convert.ToString(reader["ds_gruposocial"]);
                model.Inclusao = Convert.ToDateTime(reader["dt_inclusao"]);

                lista.Add(model);
            }
            reader.Close();

            return lista;
        }

        public Model.PensamentoModel FiltrarPorID(int id)
        {
            string script = "select * from tb_pensamento where id_pensamento = @id_pensamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pensamento", id));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            Model.PensamentoModel model = null;

            if (reader.Read())
            {
                model = new Model.PensamentoModel();
                model.Id = Convert.ToInt32(reader["id_pensamento"]);
                model.Polaridade = Convert.ToString(reader["ds_polaridade"]);
                model.Sentimento = Convert.ToString(reader["ds_sentimento"]);
                model.GrupoSocial = Convert.ToString(reader["ds_gruposocial"]);
                model.Inclusao = Convert.ToDateTime(reader["dt_inclusao"]);
            }
            reader.Close();

            return model;
        }

        public void Alterar(Model.PensamentoModel pensamento)
        {
            string script = @"update tb_pensamento
                                        set ds_polaridade = @ds_polaridade,
                                            ds_sentimento = @ds_sentimento,
                                            ds_gruposocial = @ds_gruposocial
                                      where id_pensamento = @id_pensamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pensamento", pensamento.Id));
            parms.Add(new MySqlParameter("ds_polaridade", pensamento.Polaridade));
            parms.Add(new MySqlParameter("ds_sentimento", pensamento.Sentimento));
            parms.Add(new MySqlParameter("ds_gruposocial", pensamento.GrupoSocial));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }

        public void Deletar (int id)
        {
            string script = @"delete from tb_pensamento where id_pensamento = @id_pensamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pensamento", id));

            DB db = new DB();

            db.ExecuteInsertScript(script, parms);
        }
    }
}
