﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BD.Business
{
    class PensamentoBusiness
    {
        public void Inserir(Model.PensamentoModel pensamento)
        {
            if (pensamento.Polaridade == string.Empty)
            {
                throw new ArgumentException("Polaridade é obrigatório");
            }

            if (pensamento.Sentimento == string.Empty)
            {
                throw new ArgumentException("Sentimento é obrigatório");
            }

            if (pensamento.GrupoSocial == string.Empty)
            {
                throw new ArgumentException("Grupo Social é obrigatório");
            }

            Database.PensamentoDatabase pensamentoDatabase = new Database.PensamentoDatabase();
            pensamentoDatabase.Inserir(pensamento);
        }

        public List<Model.PensamentoModel> Filtrar(string polaridade, DateTime dia)
        {
            Database.PensamentoDatabase pensamentoDatabase = new Database.PensamentoDatabase();
            List<Model.PensamentoModel> lista = pensamentoDatabase.Filtrar(polaridade, dia);

            return lista;
        }

        public Model.PensamentoModel FiltrarPorID (int id)
        {
            Database.PensamentoDatabase pensamentoDatabase = new Database.PensamentoDatabase();
            Model.PensamentoModel model = pensamentoDatabase.FiltrarPorID(id);

            return model;
        }

        public void Alterar (Model.PensamentoModel model)
        {
            if (model.Polaridade == string.Empty)
            {
                throw new ArgumentException("Polaridade é obrigatorio");
            }

            Database.PensamentoDatabase pensamentoDatabase = new Database.PensamentoDatabase();
            pensamentoDatabase.Alterar(model);
        }

        public void Deletar (int id)
        {
            Database.PensamentoDatabase pensamentoDatabase = new Database.PensamentoDatabase();
            pensamentoDatabase.Deletar(id);
        }
    }
}
