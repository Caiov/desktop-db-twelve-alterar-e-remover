﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BD.Business
{
    class FilmeBusiness
    {
        public void Inserir(Model.FilmeModel filme)
        {
            if (filme.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório");
            }

            Database.FilmeDatabase filmeDatabase = new Database.FilmeDatabase();
            filmeDatabase.Inserir(filme);
        }

        public List<Model.FilmeModel> FiltrarPorNome(string nome)
        {
            Database.FilmeDatabase filmeDatabase = new Database.FilmeDatabase();
            List<Model.FilmeModel> lista = filmeDatabase.FiltrarPorNome(nome);

            return lista;
        }
        public Model.FilmeModel FiltrarPorID(int id)
        {
            Database.FilmeDatabase filmeDatabase = new Database.FilmeDatabase();
            Model.FilmeModel model  = filmeDatabase.FiltrarPorID(id);

            return model;
        }

        public void Alterar(Model.FilmeModel filme)
        {
            if(filme.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é Obrigatorio");
            }
            
            Database.FilmeDatabase filmeDatabase = new Database.FilmeDatabase();
            filmeDatabase.Alterar(filme);
        }

        public void Remover(int id)
        {
            Database.FilmeDatabase filmeDatabase = new Database.FilmeDatabase();
            filmeDatabase.Remover(id);
        }
    }
}
