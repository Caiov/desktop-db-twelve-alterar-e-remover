﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BD.Business
{
    class AgendaBusiness
    {
        public void Inserir(Model.AgendaModel agenda)
        {
            if (agenda.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório");
            }

            Database.AgendaDatabase filmeDatabase = new Database.AgendaDatabase();
            filmeDatabase.Inserir(agenda);
        }

        public List<Model.AgendaModel> Filtrar(string nome)
        {
            Database.AgendaDatabase agendaDatabase = new Database.AgendaDatabase();
            List<Model.AgendaModel> lista = agendaDatabase.Filtrar(nome);

            return lista;
        }

        public Model.AgendaModel FiltrarPorID(int id)
        {
            Database.AgendaDatabase agendadatabase = new Database.AgendaDatabase();
            Model.AgendaModel model = agendadatabase.FiltrarPorID(id);

            return model;
        }

        public void Alterar (Model.AgendaModel agenda)
        {
            if(agenda.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é Obrigatorio");
            }

            Database.AgendaDatabase agendadatabase = new Database.AgendaDatabase();
            agendadatabase.Alterar(agenda);
        }

        public void Deletrar (int id)
        {
            Database.AgendaDatabase agendadatabase = new Database.AgendaDatabase();
            agendadatabase.Deletar(id);
        }

    }
}
